# vim: autoindent tabstop=2 shiftwidth=2 expandtab softtabstop=2 filetype=sh
# This file includes any non-shell specific code intended to be loaded in a 
# login shell.

# intentionally empty, as most of the code resides in .shellrc
