[ -n "$PS1" ] && source ~/.bash_profile;

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

if [ -e "$HOME/.tmuxinator" ]; then
	source ~/.bin/tmuxinator.bash
end
