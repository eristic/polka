" Auto-install plug.vim {{{

if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" }}}

" Install Plugged bundles
call plug#begin('~/.local/share/nvim/plugged')

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/vim-easy-align'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'editorconfig/editorconfig-vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'airblade/vim-gitgutter'
Plug 'bling/vim-airline'
Plug 'benmills/vimux'
Plug 'Shougo/neomru.vim'
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler.vim'
Plug 'vimwiki/vimwiki'

" Colors
Plug 'challenger-deep-theme/vim'

" Clojure
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'christoomey/vim-tmux-navigator', { 'for': 'clojure' }

" Elixir
Plug 'elixir-editors/vim-elixir', { 'for': 'elixir' }
Plug 'mattreduce/vim-mix', { 'for': 'elixir' }

" Javascript

Plug 'vimlab/neojs', { 'for': 'javascript' }
Plug 'elzr/vim-json', { 'for': 'json' }

" Go
Plug 'fatih/vim-go', { 'for': 'go' }

" Lua

Plug 'wolfgangmehner/lua-support', { 'for': 'lua' }

" Ruby

Plug 'tpope/vim-rails', { 'for': 'ruby' }
Plug 'thoughtbot/vim-rspec', { 'for': 'ruby' }

Plug 'jparise/vim-graphql'
Plug 'chase/vim-ansible-yaml'

" Nyaovim {{{

Plug 'rhysd/nyaovim-markdown-preview'

" }}}

" Configure Challenger Deep color scheme {{{

if has('nvim')
  Plug 'challenger-deep-theme/vim'
endif

if has('nvim') || has('termguicolors')
  set termguicolors
endif


" }}}

" Configure Janah color scheme {{{
"
" if has('nvim')
"   Plug 'mhinz/vim-janah'
" endif
"
" autocmd ColorScheme janah highlight Normal ctermbg=235
" colorscheme janah
"
" }}}
call plug#end()

colorscheme challenger_deep
let g:lightline = { 'colorscheme': 'challenger_deep'}

let mapleader = '\'

" Unite
let g:unite_source_history_yank_enable = 1

call unite#filters#matcher_default#use(['matcher_fuzzy'])

nnoremap <Leader>/ :Unite grep:.<cr>
nnoremap <C-p> :Unite file_rec/async<cr>
nnoremap <Leader>s :Unite -quick-match buffer<cr>

" Custom mappings for the unite buffer
autocmd FileType unite call s:unite_settings()
function! s:unite_settings()
  " Play nice with supertab
  let b:SuperTabDisabled=1
  " Enable navigation with control-j and control-k in insert mode
  imap <buffer> <C-j>   <Plug>(unite_select_next_line)
  imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
endfunction

nnoremap <C-\> :VimFilerExplorer<cr>
let g:vimfiler_as_default_explorer = 1

autocmd FileType vimfiler imap <buffer> <C-\> <Plug>(vimfiler_exit)
autocmd FileType vimfiler nnoremap <silent><buffer><expr> <CR> vimfiler#smart_cursor_map(
\ "\<Plug>(vimfiler_expand_tree)",
\ "\<Plug>(vimfiler_edit_file)")

let g:vimfiler_tree_leaf_icon = ' '
let g:vimfiler_tree_opened_icon = '▾'
let g:vimfiler_tree_closed_icon = '▸'
let g:vimfiler_file_icon = '-'
let g:vimfiler_marked_file_icon = '*'

