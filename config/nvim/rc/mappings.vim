nnoremap <Space> <Nop>
let mapleader="\<Space>"

"" Quick buffer switching {{{
function SwitchBuffer()
  b#
endfunction
" }}}

nmap <Space><Space> :call SwitchBuffer()<CR>

" regenerate CTAGS with ripper-tags
map <Leader>ct :silent !~/.rbenv/shims/ripper-tags -R --exclude=vendor<CR>

" resize for 80 or 130 character width
map <Leader>r8 :vertical resize 80<CR>
map <Leader>r12 :vertical resize 130<CR>

" Rubocop fix current file
nmap <leader>rc :call RubocopAutoFix()<CR>

map // :Commentary<CR>

nmap <leader>vi :tabe ~/.config/nvim/init.vim<CR>
nmap <leader>vp :tabe ~/.config/nvim/rc/plugins.vim<CR>
