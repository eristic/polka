" Help Neomake find rbenv and pyenv shims
"
" Set rbenv root.
if empty($RBENV_ROOT)
  let s:rbenv_root = $HOME . '/.rbenv'
else
  let s:rbenv_root = $RBENV_ROOT
endif

if isdirectory(s:rbenv_root)
  " Add rbenv bin to path.
  let s:rbenv_bin = s:rbenv_root . '/bin'
  let $PATH = substitute($PATH, ':' . s:rbenv_bin, '', '')
  let $PATH = s:rbenv_bin . ':' . $PATH

  " Add rbenv shims to path.
  let s:rbenv_shims = s:rbenv_root . '/shims'
  let $PATH = substitute($PATH, ':' . s:rbenv_shims, '', '')
  let $PATH = s:rbenv_shims . ':' . $PATH
endif

" Set pyenv root.
if empty($PYENV_ROOT)
  let s:pyenv_root = $HOME . '/.pyenv'
else
  let s:pyenv_root = $PYENV_ROOT
endif

if isdirectory(s:pyenv_root)
  " Add pyenv shims to path.
  let s:pyenv_shims = s:pyenv_root . '/shims'
  let $PATH = substitute($PATH, ':' . s:pyenv_shims, '', '')
  let $PATH .= ':' . s:pyenv_shims
endif

" Bonus example to set deoplete-jedi path from vim-pyenv.
augroup pyenv-deoplete-jedi-path
  autocmd!
  autocmd User vim-pyenv-activate-post
    \ let g:deoplete#sources#jedi#python_path = g:pyenv#python_exec
  autocmd User vim-pyenv-deactivate-post
    \ unlet g:deoplete#sources#jedi#python_path
augroup END
" let g:neomake_verbose = 3
let g:neomake_logfile = '/tmp/neomake.log'


let g:neomake_ruby_reek_maker_errorformat =
        \ '%E%.%#: Racc::ParseError: %f:%l :: %m,' .
        \ '%W%f:%l: %m'
let g:neomake_ruby_reek_maker = {
    \ 'args': ['--single-line'],
    \ 'errorformat': g:neomake_ruby_reek_maker_errorformat,
    \ }

let g:neomake_ruby_enabled_makers = ['mri', 'rubocop', 'reek']
let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_serialize = 1
let g:neomake_serialize_abort_on_error = 1


function! MyOnBattery()
  if filereadable('/usr/bin/pmset')
    silent exe '!pmset -g batt | grep discharging'
    return !v:shell_error
  else
    return readfile('/sys/class/power_supply/AC/online') == ['0']
  endif
endfunction
if MyOnBattery()
  call neomake#configure#automake('w')
else
  call neomake#configure#automake('inrw', 1000)
endif
