" Automatically install vim-plug if it's absent
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Note: This file will EXPLODE VIOLENTLY if loaded in regular vim, but it
" would be too difficult to sort out the neovim only plugins

" Install vim-plug bundles
call plug#begin('~/.local/share/nvim/plugged')

Plug 'junegunn/vim-easy-align'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'benekastah/neomake'
Plug 'sbdchd/neoformat', { 'do': 'brew install vint' }
Plug 'kassio/neoterm'
Plug 'autozimu/LanguageClient-neovim', {
      \ 'branch': 'next',
      \ 'do': 'bash install.sh',
      \ }

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Ctags {{{
Plug 'majutsushi/tagbar'
" }}}

Plug 'junegunn/vim-easy-align'
Plug 'janko-m/vim-test'
Plug 'KeitaNakamura/highlighter.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'itchyny/lightline.vim'
Plug 'taohex/lightline-buffer'
Plug 'tpope/vim-unimpaired'
Plug 'Shougo/neco-syntax'
Plug 'chrisbra/Colorizer'
Plug 'mhinz/vim-signify'
Plug 'emilyst/vim-xray'

Plug 'tpope/vim-rsi'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'

" Clojure
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'clojure-vim/acid.nvim', {
      \ 'for': 'clojure',
      \ 'do': 'UpdateRemotePlugins' }

" Docker
Plug 'docker/docker'
Plug 'ekalinin/Dockerfile.vim'

" Elm
Plug 'ElmCast/elm-vim'
Plug 'pbogut/deoplete-elm'

" Elixir
Plug 'slashmili/alchemist.vim'
Plug 'elixir-editors/vim-elixir', {'for': 'elixir' }

" Git
Plug 'tpope/vim-git'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'lambdalisue/vim-gista'
Plug 'rhysd/committia.vim'


" Go
Plug 'zchee/deoplete-go', { 'do': 'make', 'for': 'go' }
Plug 'fatih/vim-go', { 'for': 'go' }

" Groovy
Plug 'vim-scripts/groovy.vim', { 'for': 'groovy' }

" Java
Plug 'artur-shaik/vim-javacomplete2', { 'for': 'java' }

" Javascript
Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
Plug 'flowtype/vim-flow', {'do': 'npm install -g flow-bin'}
Plug 'othree/javascript-libraries-syntax.vim', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'kchmck/vim-coffee-script', { 'for': ['coffee', 'haml', 'eruby'] }
Plug 'mxw/vim-jsx', { 'for': 'javascript.jsx' }
Plug 'othree/jspc.vim', { 'for': ['javascript', 'javascript.jsx'] }

" Python
Plug 'zchee/deoplete-jedi', {'for': 'python' }
"
" Markdown
Plug 'junegunn/goyo.vim'
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'reedes/vim-pencil'
Plug 'ujihisa/neco-look', { 'for': ['text', 'note', 'gitcommit', 'markdown'] }

" Ruby ~/.config/nvim/rc/ruby.vim {{{

Plug 'fishbullet/deoplete-ruby'
Plug 'tpope/vim-bundler', { 'for': 'ruby' }
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails', { 'for': ['ruby', 'eruby', 'haml', 'coffee', 'javascript'] }
Plug 'tpope/vim-rbenv', { 'for': 'ruby' }
Plug 'Keithbsmiley/rspec.vim', { 'for': 'ruby' }
Plug 'thoughtbot/vim-rspec', { 'for': 'ruby' }
Plug 'tpope/vim-rake', { 'for': 'ruby' }
Plug 'tpope/vim-rbenv', { 'for': 'ruby' }
Plug 'cristianbica/neomake-rspec', { 'for': 'ruby' }

" }}}

" Rust
Plug 'rust-lang/rust.vim', { 'for': 'rust' }

" Shell
Plug 'chrisbra/vim-zsh'

" Yaml
Plug 'stephpy/vim-yaml'

Plug 'junegunn/vim-emoji'
Plug 'fszymanski/deoplete-emoji'
Plug 'Shougo/neco-vim'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'Shougo/neossh.vim'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'dysnomian/vim-pureillusion'

Plug 'rizzatti/dash.vim'

call plug#end()

" use ag for search
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
