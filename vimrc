" vim: set foldmethod=syntax:
" vim: set filetype=vim:

" 1 important ============================================================= {{{
if &compatible
  set nocompatible
endif

function! s:source_rc(path, ...) abort "{{{
  let use_global = get(a:000, 0, !has('vim_starting'))
  let abspath = resolve(expand('~/.config/nvim/rc/' . a:path))
  if !use_global
    execute 'source' fnameescape(abspath)
    return
  endif

  " substitute all 'set' to 'setglobal'
  let content = map(readfile(abspath),
        \ 'substitute(v:val, "^\\W*\\zsset\\ze\\W", "setglobal", "")')
  " create tempfile and source the tempfile
  let tempfile = tempname()
  try
    call writefile(content, tempfile)
    execute 'source' fnameescape(tempfile)
  finally
    if filereadable(tempfile)
      call delete(tempfile)
    endif
  endtry
endfunction "}}}

call s:source_rc('filetype.vim')
call s:source_rc('plugins.vim')

" ========================================================================= }}}
" 2 moving around, searching and patterns ================================= {{{
set exrc

" Don’t reset cursor to start of line when moving around.
set nostartofline

" Ignore case of searches
set ignorecase
set smartcase

" Add the g flag to search/replace by default
set gdefault

" Highlight dynamically as pattern is typed
set incsearch
set showmatch

" ========================================================================= }}}
" 3 tags ================================================================== {{{
" ========================================================================= }}}
" 4 displaying text ======================================================= {{{

" Show tab characters and trailing whitespace
set list
set list listchars=tab:»·,trail:·

" Enable line numbers
set number

" Start scrolling three lines before the horizontal window border
set scrolloff=5

" ========================================================================= }}}
" 5 syntax, highlighting and spelling ===================================== {{{

set background=dark
colorscheme pureillusion

" Enable syntax highlighting
syntax on

" Highlight searches
set hlsearch

" stop syntax highlighting this many columns out
set synmaxcol=300  

" Set true colors if the terminal supports it
if has('nvim') || has('termguicolors')
  set termguicolors
endif

let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Highlight current line
set cursorline

" Show a vertical line/guard at column 80
let &colorcolumn=join(range(81,999),',')
highlight ColorColumn ctermbg=235 guibg=#b06e80
let &colorcolumn="80,".join(range(131,999),',')

" Custom highlights
hi Comment        term=italic      cterm=italic      gui=italic
hi Keyword        term=bold        cterm=bold        gui=bold
hi Conditional    term=bold        cterm=bold        gui=bold
hi Define         term=bold        cterm=bold        gui=bold

" ========================================================================= }}}
" 6 multiple windows ====================================================== {{{

" Always show status line
set laststatus=2

" do the splits! DO THE SPLITS!
" (make vim splits work the way you'd think)
set splitbelow
set splitright

" Show the filename in the window titlebar
set title
" ========================================================================= }}}
" 9 using the mouse ======================================================= {{{

" Enable mouse in all modes
set mouse=a

" ========================================================================= }}}
" 10 GUI ================================================================== {{{
" ========================================================================= }}}
" 11 printing ============================================================= {{{
" ========================================================================= }}}
" 12 messages and info ==================================================== {{{

" unnecessary with lightline
set noshowmode
" Show the cursor position
set ruler
" Show the (partial) command as it’s being typed
set showcmd
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Disable error bells
set noerrorbells

" ========================================================================= }}}
" 13 selecting text ======================================================= {{{

" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed

" ========================================================================= }}}
" 14 editing text ========================================================= {{{

" Allow backspace in insert mode
set backspace=indent,eol,start

" ========================================================================= }}}
" 15 tabs and indenting =================================================== {{{

" Indentation
set tabstop=8
set shiftwidth=2
set expandtab
set autoindent smartindent
set softtabstop=2
set shiftround

" ========================================================================= }}}
" 16 folding ============================================================== {{{

set foldenable
set foldlevel=99      " Don't autofold anything
set foldlevelstart=99 " Don't autofold anything
set foldmethod=manual " allows faster editing by default

function! MyFoldText() " {{{
    let line = getline(v:foldstart)

    let nucolwidth = &foldcolumn + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(' ',fillcharcount) . foldedlinecount . '…' . ' '
endfunction " }}}
set foldtext=MyFoldText()

" ========================================================================= }}}
" 17 diff mode ============================================================ {{{
" ========================================================================= }}}
" 18 mapping ============================================================== {{{

call s:source_rc('mappings.vim')

" ========================================================================= }}}
" 18 mapping ============================================================== {{{

" Don’t add empty newlines at the end of files
set binary
set noendofline

" ========================================================================= }}}
" 19 reading and writing files ============================================ {{{

" Respect modeline in files
set modeline
set modelines=4

" ========================================================================= }}}
" 20 the swap file ======================================================== {{{
" ========================================================================= }}}
" 21 command line editing ================================================= {{{

" Enhance command-line completion
set wildmenu

" ========================================================================= }}}
" 22 executing external commands ========================================== {{{
" ========================================================================= }}}
" 23 running make and jumping to errors =================================== {{{
" ========================================================================= }}}
" 24 language specific ==================================================== {{{

call s:source_rc('ruby.vim')

" Plugin specific settings
call s:source_rc('pencil.vim')

let g:python_host_prog  = '/usr/local/opt/python@2/bin/python2'
let g:python3_host_prog = '/usr/local/bin/python3'

" ========================================================================= }}}
" 25 multi-byte characters ================================================ {{{

" Use UTF-8 without BOM
set encoding=utf-8 nobomb

" ========================================================================= }}}
" 26 various ============================================================== {{{
" ========================================================================= }}}

" Use UTF-8 without BOM
set encoding=utf-8 nobomb

" Automatic commands
if has('autocmd')
    " Enable file type detection
    filetype on
    " Treat .json files as .js
    autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
    " Treat .md files as Markdown
    autocmd BufNewFile,BufRead *.md setlocal filetype=markdown
endif

" plugins ================================================================= {{{

""" Plugin Configuration
call s:source_rc('deoplete.vim')
call s:source_rc('denite.vim')
call s:source_rc('emoji.vim')
call s:source_rc('goyo.vim')
call s:source_rc('highlighter.vim')
call s:source_rc('lightline.vim')
call s:source_rc('neomake.vim')
call s:source_rc('nerdtree.vim')
call s:source_rc('neoformat.vim')
call s:source_rc('neoterm.vim')
call s:source_rc('rainbow_parens.vim')
call s:source_rc('rust.vim')
call s:source_rc('searchtasks.vim')
call s:source_rc('signify.vim')
call s:source_rc('tagbar.vim')
call s:source_rc('tabline.vim')
call s:source_rc('tern.vim')
call s:source_rc('test.vim')

" ========================================================================= }}}

if has('nvim') || has('gui_running')
  autocmd! FileType fzf
  autocmd  FileType fzf set laststatus=0 | autocmd WinLeave <buffer> set laststatus=2
endif

" }}}

"" vim: set fen fdm=marker vop-=folds tw=72 :
