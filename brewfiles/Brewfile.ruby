cask_args appdir: "/Applications"

brew "git"

# Install a higher version system Ruby

brew "ruby"

# Ruby switcher

brew "rbenv"
brew "ruby-build"

# System Gemfile

system <<-HEREDOC
  eval "$(rbenv init -)"
  rbenv install 2.5.0
  gem install bundler
  bundle install
HEREDOC
